module ConditionNumberPoisson
using DrWatson
@quickactivate "GeneralizedWSBM"
using GeneralizedWSBM
using Gridap
using TimerOutputs
using Plots
using DataFrames, DataFramesMeta
using Colors

to = TimerOutput("ConditionNumberPoisson")

# Define fixed parameters
center = [0.5,0.5]
radius = 0.3
n_petals = 5
u(x) = sin(15*π*x[1])*sin(15*π*x[2])
f(x) = -tr(∇∇(u)(x))

# Warm-up parameters
ϕ = level_set(ParallelogramParams(v₁=[0.21,0.21],v₂=[0.64,0.31],v₃=[0.83,0.83],v₄=[0.37,0.73],in_out=1))
n_cells = (8,8)
output_folder = datadir("sims","ConditionNumberPoisson")
params = PoissonParams(ϕ=ϕ,f=f,u₀=u,n_cells=n_cells,output_folder=output_folder,order=2)

# Execute main function (Warm-up)
println(main_poisson(params))

# Test parameters
weight_approach = [:fraction]
verbose = false
n = [10,20,40,80]
order = [1]
ϕ_name = [:circle]
radius = collect(0.1:0.05:0.4)
all_params = @strdict weight_approach verbose n ϕ_name order radius
cases = dict_list(all_params)

# Execute case function
function execute_case(case)
  @unpack weight_approach, verbose, n, ϕ_name, order, radius = case
  case_name = savename(case,"jld2",allowedtypes=(Real, String, Symbol, Function))
  println("Executing case: ",case_name)

  # Case parameters
  if ϕ_name == :circle
    ϕ = level_set(CircleParams(center=center,radius=radius))
  elseif ϕ_name == :flower
    ϕ = level_set(FlowerParams(center=center,radius=radius,n=n_petals))
  elseif ϕ_name == :parallelogram
    ϕ = level_set(ParallelogramParams(v₁=[0.21,0.21],v₂=[0.64,0.31],v₃=[0.83,0.83],v₄=[0.37,0.73],in_out=1))
  else
    error("Case not recognized")
  end
  if weight_approach == :fraction
    λ = 0.5
  else 
    λ = 1.0
  end
  params = PoissonParams(ϕ=ϕ,f=f,u₀=u,n_cells=(n,n),weight_approach=weight_approach,
    verbose=verbose, order=order,β₁=1.0,β₂=1.0e-2,β₃=1.0e-2,λ=λ,compute_cond=true)

  # Execute main function
  results = copy(case)
  @timeit to "main_$(case_name)" results["l2norm"], results["cond"] = main_poisson(params)
  results["time"] = TimerOutputs.time(to["main_$(case_name)"])/1.0e9

  return results
end

# Execute cases
for case in cases
  path = datadir("sims","ConditionNumberPoisson")
  filename = config -> savename(config,allowedtypes=(Real, String, Symbol, Function))
  data, file = produce_or_load(path,case,execute_case;filename=filename)
end

# Get data
all_results = collect_results(datadir("sims","ConditionNumberPoisson"))

# Plot results
# Define the two endpoint colors (e.g., blue and orange for good contrast)
start_color = colorant"#4477AA"  # Blue (colorblind-friendly)
end_color   = colorant"#D55E00"  # Orange (colorblind-friendly)

# Generate 7 interpolated colors
colors = [RGB(
  (1 - t) * start_color.r + t * end_color.r,
  (1 - t) * start_color.g + t * end_color.g,
  (1 - t) * start_color.b + t * end_color.b
) for t in range(0, stop=1, length=7)]

plt = plot(xlabel="Number of elements",ylabel="κ(A)",legend=:topleft,xaxis=:log,yaxis=:log)
global plot_ref_line = true
for (i,radius) in enumerate(radius)
  global plot_ref_line 
  results = @linq all_results[
    all_results.:ϕ_name.==:circle .&& 
    all_results.:weight_approach.==:fraction .&& 
    all_results.:order.==order .&& 
    all_results.:radius.==radius,:] |> orderby(:n)
  plot!(plt,n,results.:cond,marker=:circle,label="R = $radius",color=color=colors[i],xticks = (results.:n, string.(results.:n)),lw=1.5)
  if plot_ref_line
    plot!(plt,n,4.0e-1*(n).^(4),xticks = (results.:n, string.(results.:n)),ls=:dash,color=:black,label=false,lw=1.5)
    x_triangle = [40, 50, 50]
    y_triangle = [4.0e-1*(40).^(4),4.0e-1*(40).^(4),4.0e-1*(50).^(4)]
    plot!(x_triangle, y_triangle, lw = 1, color = :black, label = "")
    annotate!(45, 2.6e-1*(40).^(4), text("1", :black, 9, :left))
    annotate!(51, 2.8e-1*(50).^(4), text("4", :black, 9, :left))
    plot_ref_line = false
  end
end        
display(plt)
savefig(plt,plotsdir("ConditionNumberPoisson","cond_circle.pdf"))

end
