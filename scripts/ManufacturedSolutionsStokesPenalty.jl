module ManufacturedSolutionsStokesPenalty
using DrWatson
@quickactivate "GeneralizedWSBM"
using GeneralizedWSBM
using Gridap
using TimerOutputs
using Plots
using DataFrames, DataFramesMeta

to = TimerOutput("ManufacturedStokesPenalty")

# Define fixed parameters
center = [0.5,0.5]
radius = 0.3
n_petals = 5
a₀ = 1/10
u₁(x) = 20*x[1]*x[2]^3
u₂(x) = 5*x[1]^4-5*x[2]^4
u(x) = VectorValue(u₁(x),u₂(x))
p(x) =  60*x[1]^2*x[2] - 20*x[2]^3
ν = 1.0
I = TensorValue(1.0,0.0,0.0,1.0)
f(x) = -ν*Δ(u)(x) + ∇(p)(x)#VectorValue(f₁(x),f₂(x)) + ∇(p)(x)


# Warm-up parameters
ϕ = level_set(CircleParams(center=center,radius=radius))
# ϕ = level_set(FlowerParams(center=center,radius=radius,n=n_petals))
n_cells = (20,20)
output_folder = datadir("sims","ManufacturedStokesPenalty")
params = StokesParams(ϕ=ϕ,f=f,u₀=u,p₀=p,ν=ν,n_cells=n_cells,output_folder=output_folder,order=1,weight_quad_degree=20)

# Execute main function (Warm-up)
println(main_stokes(params))

# Test parameters
weight_approach = [:standard]#,:binary]
verbose = false
n = [40]
order = [1]
β₁ = [1.0e-3,1.0,1.0e3,1.0e-6]
β₂ = [1.0e-3,1.0,1.0e3,1.0e-6]
β₃ = [1.0e-3,1.0,1.0e3,1.0e-6]
ϕ_name = [:flower]
all_params = @strdict weight_approach verbose n ϕ_name order β₁ β₂ β₃
cases = dict_list(all_params)

# Execute case function
function execute_case(case)
  @unpack weight_approach, verbose, n, ϕ_name, order, β₁, β₂, β₃ = case
  case_name = savename(case,"jld2",allowedtypes=(Real, String, Symbol, Function))
  println("Executing case: ",case_name)

  # Case parameters
  if ϕ_name == :circle
    ϕ = level_set(CircleParams(center=center,radius=radius))
  elseif ϕ_name == :flower
    ϕ = level_set(FlowerParams(center=center,radius=radius,n=n_petals))
  elseif ϕ_name == :parallelogram
    ϕ = level_set(ParallelogramParams(v₁=[0.21,0.21],v₂=[0.64,0.31],v₃=[0.83,0.83],v₄=[0.37,0.73],in_out=1))
  else
    error("Case not recognized")
  end
  params = StokesParams(ϕ=ϕ,f=f,u₀=u,n_cells=(n,n),weight_approach=weight_approach,
    verbose=verbose, order=order, β₁=β₁, β₂=β₂, β₃=β₃)

  # Execute main function
  results = copy(case)
  @timeit to "main_$(case_name)" results["l2normᵤ"], results["l2normₚ"] = main_stokes(params)
  results["time"] = TimerOutputs.time(to["main_$(case_name)"])/1.0e9

  return results
end

# Execute cases
for case in cases
  path = datadir("sims","ManufacturedStokesPenalty")
  filename = config -> savename(config,allowedtypes=(Real, String, Symbol, Function))
  data, file = produce_or_load(path,case,execute_case;filename=filename)
end

# Get data
all_results = collect_results(datadir("sims","ManufacturedStokesPenalty"))
plot_geom_cases = [:flower]
plot_approaches = [:standard]
plot_orders = [1]
plot_β₁ = [1.0e-3,1.0,1.0e3,1.0e-6]
plot_β₂ = [1.0e-3,1.0,1.0e3,1.0e-6]
plot_β₃ = [1.0e-3,1.0,1.0e3,1.0e-6]

# Plot results
markers = [:circle,:square,:diamond,:cross]
lines = [:solid,:dash,:dashdot]
colors = [:green,:red,:blue,:black]
filenames = ["l2norm_flower.pdf"]
for (igeom, geom_case) in enumerate(plot_geom_cases)
  plt = plot(xlabel="β₃",ylabel="L² error",legend=:topleft,xaxis=:log,yaxis=:log)
  plot_ref_line = true
  for (iβ₁,β₁) in enumerate(plot_β₁)
    for (iβ₂,β₂) in enumerate(plot_β₂)
      vals = []
      for (iβ₃,β₃) in enumerate(plot_β₃)
        for (iorder, order) in enumerate(plot_orders)
          for (iapproach, approach) in enumerate(plot_approaches)
            results = @linq all_results[all_results.:ϕ_name.==geom_case .&&
                            all_results.:weight_approach.==approach .&&
                            all_results.:order.==order .&&
                            all_results.:β₁.==β₁ .&&
                            all_results.:β₂.==β₂ .&&
                            all_results.:β₃.==β₃ ,:] |> orderby(:n)
            push!(vals,results.:l2normᵤ[1])
          end
        end
      end
      plot!(plt,plot_β₃,vals,marker=markers[iβ₁],color=colors[iβ₂],label="β₁=$(β₁), β₂=$(β₂)")
    end
  end
  savefig(plt,plotsdir("ManufacturedStokesPenalty",filenames[igeom]))
end

end
