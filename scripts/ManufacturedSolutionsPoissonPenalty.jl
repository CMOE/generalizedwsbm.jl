module ManufacturedSolutionsPoissonPenalty
using DrWatson
@quickactivate "GeneralizedWSBM"
using GeneralizedWSBM
using Gridap
using TimerOutputs
using Plots
using DataFrames, DataFramesMeta

to = TimerOutput("ManufacturedPoissonPenalty")

# Define fixed parameters
center = [0.5,0.5]
radius = 0.25
n_petals = 5
u(x) = x[1]^3*x[2]^2#sin(4π*x[1])*cos(8π*x[2])
f(x) = -tr(∇∇(u)(x))

# Warm-up parameters
# ϕ = level_set(CircleParams(center=center,radius=radius))
ϕ = level_set(ParallelogramParams(v₁=[0.21,0.21],v₂=[0.64,0.31],v₃=[0.83,0.83],v₄=[0.37,0.73],in_out=1))
n_cells = (8,8)
output_folder = datadir("sims","ManufacturedPoissonPenalty")
params = PoissonParams(ϕ=ϕ,f=f,u₀=u,n_cells=n_cells,output_folder=output_folder,order=2)

# Execute main function (Warm-up)
println(main_poisson(params))

# Test parameters
weight_approach = [:standard]#,:binary]
verbose = false
n = [20]
order = [2]
β₁ = [1.0e-3,1.0,1.0e3,1.0e-6]
β₂ = [1.0e-3,1.0,1.0e3,1.0e-6]
β₃ = [1.0e-3,1.0,1.0e3,1.0e-6]
ϕ_name = [:flower]
all_params = @strdict weight_approach verbose n ϕ_name order β₁ β₂ β₃
cases = dict_list(all_params)

# Execute case function
function execute_case(case)
  @unpack weight_approach, verbose, n, ϕ_name, order, β₁, β₂, β₃ = case
  case_name = savename(case,"jld2",allowedtypes=(Real, String, Symbol, Function))
  println("Executing case: ",case_name)

  # Case parameters
  if ϕ_name == :circle
    ϕ = level_set(CircleParams(center=center,radius=radius))
  elseif ϕ_name == :flower
    ϕ = level_set(FlowerParams(center=center,radius=radius,n=n_petals))
  elseif ϕ_name == :parallelogram
    ϕ = level_set(ParallelogramParams(v₁=[0.21,0.21],v₂=[0.64,0.31],v₃=[0.83,0.83],v₄=[0.37,0.73],in_out=1))
  else
    error("Case not recognized")
  end
  params = PoissonParams(ϕ=ϕ,f=f,u₀=u,n_cells=(n,n),weight_approach=weight_approach,
    verbose=verbose, order=order, β₁=β₁, β₂=β₂, β₃=β₃)

  # Execute main function
  results = copy(case)
  @timeit to "main_$(case_name)" results["l2norm"] = main_poisson(params)
  results["time"] = TimerOutputs.time(to["main_$(case_name)"])/1.0e9

  return results
end

# Execute cases
for case in cases
  path = datadir("sims","ManufacturedPoissonPenalty")
  filename = config -> savename(config,allowedtypes=(Real, String, Symbol, Function))
  data, file = produce_or_load(path,case,execute_case;filename=filename)
end

# Get data
all_results = collect_results(datadir("sims","ManufacturedPoissonPenalty"))
plot_geom_cases = [:flower]
plot_approaches = [:standard]
plot_orders = [2]
plot_β₁ = [1.0e-3,1.0,1.0e3,1.0e-6]
plot_β₂ = [1.0e-3,1.0,1.0e3,1.0e-6]
plot_β₃ = [1.0e-3,1.0,1.0e3,1.0e-6]

# Plot results
markers = [:circle,:square,:diamond,:cross]
lines = [:solid,:dash,:dashdot]
colors = [:green,:red,:blue,:black]
filenames = ["l2norm_flower.pdf"]
for (igeom, geom_case) in enumerate(plot_geom_cases)
  plt = plot(xlabel="β₃",ylabel="L² error",legend=:topleft,xaxis=:log,yaxis=:log)
  plot_ref_line = true
  for (iβ₁,β₁) in enumerate(plot_β₁)
    for (iβ₂,β₂) in enumerate(plot_β₂)
      vals = []
      for (iβ₃,β₃) in enumerate(plot_β₃)
        for (iorder, order) in enumerate(plot_orders)
          for (iapproach, approach) in enumerate(plot_approaches)
            results = @linq all_results[all_results.:ϕ_name.==geom_case .&&
                            all_results.:weight_approach.==approach .&&
                            all_results.:order.==order .&&
                            all_results.:β₁.==β₁ .&&
                            all_results.:β₂.==β₂ .&&
                            all_results.:β₃.==β₃ ,:] |> orderby(:n)
            push!(vals,results.:l2norm[1])
          end
        end
      end
      plot!(plt,plot_β₃,vals,marker=markers[iβ₁],color=colors[iβ₂],label="β₁=$(β₁), β₂=$(β₂)")
    end
  end
  savefig(plt,plotsdir("ManufacturedPoissonPenalty",filenames[igeom]))
end

# circle_results_sbm = @linq all_results[all_results.:ϕ_name.==:circle .&& all_results.:weight_approach.==:binary,:] |> orderby(:n)
# circle_results_wsbm = @linq all_results[all_results.:ϕ_name.==:circle .&& all_results.:weight_approach.==:standard,:] |> orderby(:n)
# flower_results_sbm = @linq all_results[all_results.:ϕ_name.==:flower .&& all_results.:weight_approach.==:binary,:] |> orderby(:n)
# flower_results_wsbm = @linq all_results[all_results.:ϕ_name.==:flower .&& all_results.:weight_approach.==:standard,:] |> orderby(:n)
# parallelogram_results_sbm = @linq all_results[all_results.:ϕ_name.==:parallelogram .&& all_results.:weight_approach.==:binary,:] |> orderby(:n)
# parallelogram_results_wsbm = @linq all_results[all_results.:ϕ_name.==:parallelogram .&& all_results.:weight_approach.==:standard,:] |> orderby(:n)
# l2_circle_sbm = circle_results_sbm.:l2norm
# n_circle_sbm = circle_results_sbm.:n
# t_circle_sbm = circle_results_sbm.:time
# l2_circle_wsbm = circle_results_wsbm.:l2norm
# n_circle_wsbm = circle_results_wsbm.:n
# t_circle_wsbm = circle_results_wsbm.:time
# l2_flower_sbm = flower_results_sbm.:l2norm
# n_flower_sbm = flower_results_sbm.:n
# t_flower_sbm = flower_results_sbm.:time
# l2_flower_wsbm = flower_results_wsbm.:l2norm
# n_flower_wsbm = flower_results_wsbm.:n
# t_flower_wsbm = flower_results_wsbm.:time
# l2_parallelogram_sbm = parallelogram_results_sbm.:l2norm
# n_parallelogram_sbm = parallelogram_results_sbm.:n
# t_parallelogram_sbm = parallelogram_results_sbm.:time
# l2_parallelogram_wsbm = parallelogram_results_wsbm.:l2norm
# n_parallelogram_wsbm = parallelogram_results_wsbm.:n
# t_parallelogram_wsbm = parallelogram_results_wsbm.:time

# # Plot results
# plt1 = plot(xlabel="h",ylabel="L² error",xlims=(1.0e-3,1.0e-1),legend=:topleft)
# plot!(plt1,1.0./n_circle_sbm,l2_circle_sbm,marker=:circle,label="SBM",xaxis=:log,yaxis=:log)
# plot!(plt1,1.0./n_circle_wsbm,l2_circle_wsbm,marker=:square,label="WSBM",xaxis=:log,yaxis=:log)
# plot!(plt1,1.0./n_circle_sbm,20*(1.0./n_circle_sbm).^(2),ls=:dash,color=:black,label="h²",xaxis=:log,yaxis=:log)
# plt2 = plot(xlabel="h",ylabel="L² error",xlims=(1.0e-3,1.0e-1),legend=:topleft)
# plot!(plt2,1.0./n_flower_sbm,l2_flower_sbm,marker=:circle,label="SBM",xaxis=:log,yaxis=:log)
# plot!(plt2,1.0./n_flower_wsbm,l2_flower_wsbm,marker=:square,label="WSBM",xaxis=:log,yaxis=:log)
# plot!(plt2,1.0./n_flower_sbm,20*(1.0./n_flower_sbm).^(2),ls=:dash,color=:black,label="h²",xaxis=:log,yaxis=:log)
# plt3 = plot(xlabel="h",ylabel="L² error",xlims=(1.0e-3,1.0e-1),legend=:topleft)
# plot!(plt3,1.0./n_parallelogram_sbm,l2_parallelogram_sbm,marker=:circle,label="SBM",xaxis=:log,yaxis=:log)
# plot!(plt3,1.0./n_parallelogram_wsbm,l2_parallelogram_wsbm,marker=:square,label="WSBM",xaxis=:log,yaxis=:log)
# plot!(plt3,1.0./n_parallelogram_sbm,20*(1.0./n_parallelogram_sbm).^(2),ls=:dash,color=:black,label="h²",xaxis=:log,yaxis=:log)
# plt4 = plot(xlabel="t",ylabel="L² error",legend=:bottomleft)
# plot!(plt4,t_circle_sbm,l2_circle_sbm,marker=:circle,label="SBM",xaxis=:log,yaxis=:log)
# plot!(plt4,t_circle_wsbm,l2_circle_wsbm,marker=:square,label="WSBM",xaxis=:log,yaxis=:log)
# plt5 = plot(xlabel="t",ylabel="L² error",legend=:bottomleft)
# plot!(plt5,t_flower_sbm,l2_flower_sbm,marker=:circle,label="SBM",xaxis=:log,yaxis=:log)
# plot!(plt5,t_flower_wsbm,l2_flower_wsbm,marker=:square,label="WSBM",xaxis=:log,yaxis=:log)
# savefig(plt1,plotsdir("ManufacturedPoisson","l2norm_circle.pdf"))
# savefig(plt2,plotsdir("ManufacturedPoisson","l2norm_flower.pdf"))
# savefig(plt3,plotsdir("ManufacturedPoisson","l2norm_parallelogram.pdf"))
# savefig(plt4,plotsdir("ManufacturedPoisson","time_circle.pdf"))
# savefig(plt5,plotsdir("ManufacturedPoisson","time_flower.pdf"))


# flower_results_wsbm_wqd2 = @linq all_results[all_results.:weight_quad_degree.==2 .&& all_results.:weight_approach.==:standard,:] |> orderby(:n)
# flower_results_wsbm_wqd4 = @linq all_results[all_results.:weight_quad_degree.==4 .&& all_results.:weight_approach.==:standard,:] |> orderby(:n)
# flower_results_wsbm_wqd8 = @linq all_results[all_results.:weight_quad_degree.==8 .&& all_results.:weight_approach.==:standard,:] |> orderby(:n)
# flower_results_wsbm_wqd16 = @linq all_results[all_results.:weight_quad_degree.==16 .&& all_results.:weight_approach.==:standard,:] |> orderby(:n)

# flower_results_sbm_wqd2 = @linq all_results[all_results.:weight_quad_degree.==2 .&& all_results.:weight_approach.==:binary,:] |> orderby(:n)
# flower_results_sbm_wqd4 = @linq all_results[all_results.:weight_quad_degree.==4 .&& all_results.:weight_approach.==:binary,:] |> orderby(:n)
# flower_results_sbm_wqd8 = @linq all_results[all_results.:weight_quad_degree.==8 .&& all_results.:weight_approach.==:binary,:] |> orderby(:n)
# flower_results_sbm_wqd16 = @linq all_results[all_results.:weight_quad_degree.==16 .&& all_results.:weight_approach.==:binary,:] |> orderby(:n)

# l2_flower_wsbm_wqd2 = flower_results_wsbm_wqd2.:l2norm
# n_flower_wsbm_wqd2 = flower_results_wsbm_wqd2.:n
# t_flower_wsbm_wqd2 = flower_results_wsbm_wqd2.:time
# l2_flower_wsbm_wqd4 = flower_results_wsbm_wqd4.:l2norm
# n_flower_wsbm_wqd4 = flower_results_wsbm_wqd4.:n
# t_flower_wsbm_wqd4 = flower_results_wsbm_wqd4.:time
# l2_flower_wsbm_wqd8 = flower_results_wsbm_wqd8.:l2norm
# n_flower_wsbm_wqd8 = flower_results_wsbm_wqd8.:n
# t_flower_wsbm_wqd8 = flower_results_wsbm_wqd8.:time
# l2_flower_wsbm_wqd16 = flower_results_wsbm_wqd16.:l2norm
# n_flower_wsbm_wqd16 = flower_results_wsbm_wqd16.:n
# t_flower_wsbm_wqd16 = flower_results_wsbm_wqd16.:time

# l2_flower_sbm_wqd2 = flower_results_sbm_wqd2.:l2norm
# n_flower_sbm_wqd2 = flower_results_sbm_wqd2.:n
# t_flower_sbm_wqd2 = flower_results_sbm_wqd2.:time
# l2_flower_sbm_wqd4 = flower_results_sbm_wqd4.:l2norm
# n_flower_sbm_wqd4 = flower_results_sbm_wqd4.:n
# t_flower_sbm_wqd4 = flower_results_sbm_wqd4.:time
# l2_flower_sbm_wqd8 = flower_results_sbm_wqd8.:l2norm
# n_flower_sbm_wqd8 = flower_results_sbm_wqd8.:n
# t_flower_sbm_wqd8 = flower_results_sbm_wqd8.:time
# l2_flower_sbm_wqd16 = flower_results_sbm_wqd16.:l2norm
# n_flower_sbm_wqd16 = flower_results_sbm_wqd16.:n
# t_flower_sbm_wqd16 = flower_results_sbm_wqd16.:time

# plt = plot(1.0./n_flower_wsbm_wqd2,l2_flower_wsbm_wqd2,marker=:square,label="WSBM - 2",xaxis=:log,yaxis=:log)
# plot!(plt,1.0./n_flower_wsbm_wqd4,l2_flower_wsbm_wqd4,marker=:square,label="WSBM - 4",xaxis=:log,yaxis=:log)
# plot!(plt,1.0./n_flower_wsbm_wqd8,l2_flower_wsbm_wqd8,marker=:square,label="WSBM - 8",xaxis=:log,yaxis=:log)
# plot!(plt,1.0./n_flower_wsbm_wqd16,l2_flower_wsbm_wqd16,marker=:square,label="WSBM - 16",xaxis=:log,yaxis=:log)
# plot!(plt,1.0./n_flower_sbm_wqd2,l2_flower_sbm_wqd2,marker=:circle,label="SBM - 2",xaxis=:log,yaxis=:log)
# plot!(plt,1.0./n_flower_sbm_wqd4,l2_flower_sbm_wqd4,marker=:circle,label="SBM - 4",xaxis=:log,yaxis=:log)
# plot!(plt,1.0./n_flower_sbm_wqd8,l2_flower_sbm_wqd8,marker=:circle,label="SBM - 8",xaxis=:log,yaxis=:log)
# plot!(plt,1.0./n_flower_sbm_wqd16,l2_flower_sbm_wqd16,marker=:circle,label="SBM - 16",xaxis=:log,yaxis=:log)
# plot!(plt,1.0./n_flower_wsbm_wqd2,20*(1.0./n_flower_wsbm_wqd2).^(2),ls=:dash,color=:black,label="h²",xaxis=:log,yaxis=:log)
# display(plt)

# # VTK results circle case
# ϕ = level_set(CircleParams(center=center,radius=radius))
# n_cells = (40,40)
# output_folder = datadir("sims","ManufacturedPoisson","VTK_circle")
# params = PoissonParams(ϕ=ϕ,f=f,u₀=u,n_cells=n_cells,output_folder=output_folder)
# main_poisson(params)

# # VTK results flower case
# ϕ = level_set(FlowerParams(center=center,radius=radius,n=n_petals))
# n_cells = (40,40)
# output_folder = datadir("sims","ManufacturedPoisson","VTK_flower")
# params = PoissonParams(ϕ=ϕ,f=f,u₀=u,n_cells=n_cells,output_folder=output_folder)
# main_poisson(params)

# VTK results parallelogram case
ϕ = level_set(ParallelogramParams(v₁=[0.21,0.21],v₂=[0.64,0.31],v₃=[0.83,0.83],v₄=[0.37,0.73],in_out=1))
n_cells = (40,40)
output_folder = datadir("sims","ManufacturedPoisson","VTK_parallelogram")
params = PoissonParams(ϕ=ϕ,f=f,u₀=u,n_cells=n_cells,output_folder=output_folder,order=2)
main_poisson(params)

end
