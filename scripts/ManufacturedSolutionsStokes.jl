module ManufacturedSolutionsStokes
using DrWatson
@quickactivate "GeneralizedWSBM"
using GeneralizedWSBM
using Gridap
using TimerOutputs
using Plots
using DataFrames, DataFramesMeta

to = TimerOutput("ManufacturedStokes")

# Define fixed parameters
center = [0.5,0.5]
radius = 0.3
n_petals = 5
a₀ = 1/10
u₁(x) = 20*x[1]*x[2]^3
u₂(x) = 5*x[1]^4-5*x[2]^4
u(x) = VectorValue(u₁(x),u₂(x))
p(x) =  60*x[1]^2*x[2] - 20*x[2]^3
ν = 1.0
I = TensorValue(1.0,0.0,0.0,1.0)
f(x) = -ν*Δ(u)(x) + ∇(p)(x)#VectorValue(f₁(x),f₂(x)) + ∇(p)(x)
nout = VectorValue(1.0,0.0)
εn(x) = nout⋅ε(u)(x)
g(x) = 2*ν*εn(x) - p(x)*nout
# function g(n)
#   return g(x) = 2ν*ε(u)(x)⋅n - p(x)*n
# end

# Warm-up parameters
ϕ = level_set(CircleParams(center=center,radius=radius,in_out=1))
# ϕ = level_set(FlowerParams(center=center,radius=radius,n=n_petals))
n_cells = (10,10)
output_folder = datadir("sims","ManufacturedStokes")
params = StokesParams(ϕ=ϕ,f=f,g=g,u₀=u,p₀=p,ν=ν,n_cells=n_cells,output_folder=output_folder,order=2,weight_quad_degree=20)

# Execute main function (Warm-up)
println(main_stokes(params))

# Test parameters
weight_approach = [:standard,:binary,:fraction]
verbose = true
n = [10,20,40,80,160]#,320]
ϕ_name = [:circle,:flower,:parallelogram]
order = [2]#,2]
all_params = @strdict weight_approach verbose n ϕ_name order
cases = dict_list(all_params)

# Execute case function
function execute_case(case)
  @unpack weight_approach, verbose, n, ϕ_name, order = case
  case_name = savename(case,"jld2",allowedtypes=(Real, String, Symbol, Function))
  println("Executing case: ",case_name)

  # Case parameters
  if ϕ_name == :circle
    ϕ = level_set(CircleParams(center=center,radius=radius,in_out=1))
  elseif ϕ_name == :flower
    ϕ = level_set(FlowerParams(center=center,radius=radius,n=n_petals,in_out=1))
  elseif ϕ_name == :parallelogram
    ϕ = level_set(ParallelogramParams(v₁=[0.21,0.21],v₂=[0.64,0.31],v₃=[0.83,0.83],v₄=[0.37,0.73],in_out=-1))
  else
    error("Case not recognized")
  end
  if weight_approach == :fraction
    λ = 0.5
  else 
    λ = 1.0
  end
  params = StokesParams(ϕ=ϕ,f=f,g=g,u₀=u,p₀=p,ν=ν,n_cells=(n,n),weight_approach=weight_approach,
    verbose=verbose,order=order,β₁=1.0,β₂=1.0e-2,β₃=1.0e-2,output_folder=datadir("sims","ManufacturedStokes"),λ=λ)

  # Execute main function
  results = copy(case)
  @timeit to "main_$(case_name)" results["l2normᵤ"], results["l2normₚ"] = main_stokes(params)
  results["time"] = TimerOutputs.time(to["main_$(case_name)"])/1.0e9

  return results
end

# Execute cases
for case in cases
  path = datadir("sims","ManufacturedStokes")
  filename = config -> savename(config,allowedtypes=(Real, String, Symbol, Function))
  data, file = produce_or_load(path,case,execute_case;filename=filename)
end

# Get data
all_results = collect_results(datadir("sims","ManufacturedStokes"))
plot_geom_cases = [:circle,:flower,:parallelogram]
plot_approaches = [:standard,:binary,:fraction]
plot_orders = [2]#,2]

# Plot results
labels = ["WSBM","SBM","λ-SBM"]
markers = [:circle,:square,:utriangle]
lines = [:solid,:dash]
colors = ["#0072B2", "#E69F00", "#009E73"]
filenames_u = ["l2norm_circle_u.pdf","l2norm_flower_u.pdf","l2norm_parallelogram_u.pdf"]
filenames_p = ["l2norm_circle_p.pdf","l2norm_flower_p.pdf","l2norm_parallelogram_p.pdf"]
shift = 1.0e3
for (igeom, geom_case) in enumerate(plot_geom_cases)
  plt = plot(xlabel="Number of elements",ylabel="L² error",legend=:topright,xaxis=:log,yaxis=:log)
  plt2 = plot(xlabel="Number of elements",ylabel="L² error",legend=:topright,xaxis=:log,yaxis=:log)
  plot_ref_line = true
  for (iorder, order) in enumerate(plot_orders)
    for (iapproach, approach) in enumerate(plot_approaches)
      results = @linq all_results[all_results.:ϕ_name.==geom_case .&& all_results.:weight_approach.==approach .&& all_results.:order.==order,:] |> orderby(:n)
      plot!(plt,results.:n,results.:l2normᵤ,marker=markers[iapproach],ls=lines[iorder],color=colors[iapproach],label=labels[iapproach])
      plot!(plt2,results.:n,results.:l2normₚ,marker=markers[iapproach],ls=lines[iorder],color=colors[iapproach],label=labels[iapproach])
      if plot_ref_line
        plot!(plt,results.:n,shift*(results.:n).^(-3),xticks = (results.:n, string.(results.:n)),ls=:dashdot,color=:black,label=false)
        plot!(plt2,results.:n,shift*(results.:n).^(-2),xticks = (results.:n, string.(results.:n)),ls=:dashdot,color=:black,label=false)
        x_triangle = [80, 100, 100]
        y_triangle = [shift*(80).^(-3),shift*(80).^(-3),shift*(100).^(-3)]
        y_triangle2 = [shift*(80).^(-2),shift*(80).^(-2),shift*(100).^(-2)]
        plot!(plt,x_triangle, y_triangle, lw = 1, color = :black, label = "")
        annotate!(90, 1.2*shift*(80).^(-3), text("1", :black, 9, :left))
        annotate!(103, shift*(90).^(-3), text("3", :black, 9, :left))
        plot!(plt2,x_triangle, y_triangle2, lw = 1, color = :black, label = "")
        annotate!(90, 1.2*shift*(80).^(-2), text("1", :black, 9, :left))
        annotate!(103, shift*(90).^(-2), text("2", :black, 9, :left))
        plot_ref_line = false
      end
    end
  end
  savefig(plt,plotsdir("ManufacturedStokes",filenames_u[igeom]))
  savefig(plt2,plotsdir("ManufacturedStokes",filenames_p[igeom]))
end
# plt4 = plot(xlabel="t",ylabel="L² error",legend=:bottomleft)
# plot!(plt4,t_circle_sbm,l2_circle_sbm,marker=:circle,label="SBM",xaxis=:log,yaxis=:log)
# plot!(plt4,t_circle_wsbm,l2_circle_wsbm,marker=:square,label="WSBM",xaxis=:log,yaxis=:log)
# plt5 = plot(xlabel="t",ylabel="L² error",legend=:bottomleft)
# plot!(plt5,t_flower_sbm,l2_flower_sbm,marker=:circle,label="SBM",xaxis=:log,yaxis=:log)
# plot!(plt5,t_flower_wsbm,l2_flower_wsbm,marker=:square,label="WSBM",xaxis=:log,yaxis=:log)
# savefig(plt1,plotsdir("ManufacturedStokes","l2norm_circle.pdf"))
# savefig(plt2,plotsdir("ManufacturedStokes","l2norm_flower.pdf"))
# savefig(plt3,plotsdir("ManufacturedStokes","l2norm_parallelogram.pdf"))
# savefig(plt4,plotsdir("ManufacturedStokes","time_circle.pdf"))
# savefig(plt5,plotsdir("ManufacturedStokes","time_flower.pdf"))

# # VTK results circle case
# ϕ = level_set(CircleParams(center=center,radius=radius))
# n_cells = (160,160)
# output_folder = datadir("sims","ManufacturedStokes","VTK_circle")
# params = StokesParams(ϕ=ϕ,f=f,u₀=u,p₀=p,ν=ν,order=2,n_cells=n_cells,output_folder=output_folder,β₁=1.0,β₂=1.0e-2,β₃=1.0e-2,weight_approach=:fraction,λ=0.5)
# println(main_stokes(params))

# VTK results flower case
ϕ = level_set(FlowerParams(center=center,radius=radius,n=n_petals,in_out=1))
n_cells = (80,80)
output_folder = datadir("sims","ManufacturedStokes","VTK_flower")
params = StokesParams(ϕ=ϕ,f=f,g=g,u₀=u,p₀=p,ν=ν,order=2,n_cells=n_cells,output_folder=output_folder,β₁=1.0,β₂=1.0e-2,β₃=1.0e-2,weight_approach=:standard)
main_stokes(params)

# # VTK results parallelogram case
# ϕ = level_set(ParallelogramParams(v₁=[0.21,0.21],v₂=[0.64,0.31],v₃=[0.83,0.83],v₄=[0.37,0.73],in_out=1))
# n_cells = (40,40)
# output_folder = datadir("sims","ManufacturedStokes","VTK_parallelogram")
# params = StokesParams(ϕ=ϕ,f=f,u₀=u,p₀=p,ν=ν,n_cells=n_cells,output_folder=output_folder,weight_quad_degree=8)
# main_stokes(params)

end
