using Pkg; Pkg.activate(".")
module DistanceTest

using GeneralizedWSBM
using Gridap
domain=(0,1,0,1)
n_cells=(3,3)
model = CartesianDiscreteModel(domain,n_cells)
Ω = Interior(model)
Λ = Skeleton(Ω)
ϕ = level_set(ParallelogramParams(v₁=[-10,-10],v₂=[0.51,-10],v₃=[0.51,10],v₄=[-10,10],in_out=1))
params = PoissonParams(ϕ=ϕ)
α = compute_weights(Ω,params)
dΩ = Measure(Ω,0)
dΛ = Measure(Λ,0)

dcf = CellField(GeneralizedWSBM.d(params),Ω)
println(evaluate(dcf,get_cell_points(dΛ)))
function f(dcf)
  println(dcf)
  dcf
end
a = ∑(∫(f∘(dcf))dΛ)
le = 1/3
le2 = le/2
println(0.51-0.0," ",0.51-le2," ",0.51-(le)," ",0.51-(le+le2)," ",0.51-(2le)," ",0.51-(2le+le2))

end
