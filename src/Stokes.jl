"""
StokesParams

Parameters for the linear stokes problem.
"""
@with_kw struct StokesParams
  domain::Tuple{Float64,Float64,Float64,Float64} = (0.0,1.0,0.0,1.0)
  n_cells::Tuple{Int64,Int64} = (10,10)
  in_out_wall_tags::Tuple = ([7],[8],[1,2,3,4,5,6])
  ϕ::Function = level_set(CircleParams())
  order::Int64 = 1 # FE order
  weight_quad_degree::Int64 = 20 # Quadrature degree for computing weights
  weight_min_value::Float64 = 0.0 # Minimum value for weights
  weight_approach::Symbol = :standard # Approach for computing weights (:standard or :binary (0.0 or 1.0))
  output_folder::String = datadir("sims","Stokes") # Output folder
  λ::Float64 = 0.5 # Fraction for the "weight_approach" :fraction case
  verbose::Bool = true # Verbosity
  f::Function = x -> VectorValue(0.0,0.0) # Source term
  g::Function = n -> VectorValue(0.0,0.0) # Neumann boundary condition
  u₀::Function = x -> VectorValue(0.0,0.0) # Exact solution velocity
  p₀::Function = x -> 0.0 # Exact solution pressure
  ν::Float64 = 1.0 # viscosity
  β₁::Float64 = 1.0 # Penalty parameter
  β₂::Float64 = 1.0 # Penalty parameter
  β₃::Float64 = 1.0 # Penalty parameter
  Cinv::Vector{Float64} = [36.0,36.0] # Inverse estimate constant per order
end

"""
    main_stokes()

Main function for the stokes problem. It computes the solution to the stokes problem
on an arbitrary shaped domain with Dirichlet boundary conditions. The solution is computed using
the GeneralizedWSBM method, assuming the boundary is defined through a level set function.
"""
function main_stokes(params::StokesParams)

  # General parameters
  @unpack output_folder, verbose = params

  # Discrete model
  @unpack domain, n_cells, in_out_wall_tags = params
  model = CartesianDiscreteModel(domain,n_cells)
  Ω = Interior(model)
  Λ = Skeleton(Ω)
  labels = get_face_labeling(model)
  add_tag_from_tags!(labels,"inlet",in_out_wall_tags[1])
  add_tag_from_tags!(labels,"outlet",in_out_wall_tags[2])
  add_tag_from_tags!(labels,"wall",in_out_wall_tags[3])
  Γ_out = Boundary(Ω,tags="outlet")

  # Compute weights
  α = compute_weights(Ω,params)
  params_ref = reconstruct(params,weight_approach=:binary)
  α_ref = compute_weights(Ω,params_ref)

  # Define FE spaces
  @unpack order = params
  @unpack f, g, u₀, p₀, ν = params
  D = num_dims(Ω)
  reffeᵤ = ReferenceFE(lagrangian,VectorValue{D,Float64},order)
  reffeₚ = ReferenceFE(lagrangian,Float64,order-1)#,space=:P)
  V = TestFESpace(Ω,reffeᵤ,conformity=:H1,dirichlet_tags=["inlet","wall"])
  Q = TestFESpace(Ω,reffeₚ,conformity=:C0)#,constraint=:zeromean)
  U = TrialFESpace(V,[u₀,u₀])
  P = TrialFESpace(Q)
  Y = MultiFieldFESpace([V,Q])
  X = MultiFieldFESpace([U,P])

  # Define measures
  dΩ = Measure(Ω,2*order)
  dΛ = Measure(Λ,2*order)
  nΛ = get_normal_vector(Λ)
  dΓ = Measure(Γ_out,2*order)
  nΓ = get_normal_vector(Γ_out)

  # Auxiliar variables
  Id = TensorValue(Matrix(1.0I,D,D))
  σ(ε) = 2ν*ε
  h = mean(CellField(lazy_map(vol->(vol)^(1/D),get_cell_measure(Ω)),Ω))
  h3 = mean(CellField(lazy_map(vol->((vol)^(1/D))^3,get_cell_measure(Ω)),Ω))
  αₚ = 1/ν * (mean(α) .< 1.0) * ( abs(jump(α)) .!= 2*mean(α) )
  αₑ = 1/ν * (mean(α).== jump(α))
  dcf = CellField(d(params),Ω)
  u₀d = CellField(x->u₀(x+d(params)(x)),Ω)
  sᵤ(u) = u+dcf⋅∇(u)+1/2*(dcf⋅(dcf⋅∇∇(u)))
  sᵤ¹(u) = u+dcf⋅∇(u)

  # Define variational problem
  @unpack β₁, β₂, β₃, Cinv = params
  η = 1/(8*√(Cinv[order]))*(-4+√(Cinv[order])+(65*Cinv[order]+56*√(Cinv[order])+16)^(1/2))
  γ = β₁*(order+1)^2*η*ν # rectangles
  # γ = β₁*(order+1)*(order+2)/2*η*ν #triangres
  # a((u,p),(v,q)) =
  #   ∫( α*( σ(∇(u))⊙∇(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ + ∫( α*( q*(∇⋅u) ) )dΩ -
  #   ∫( meanᵧ(α,σ(∇(u))) ⊙ jump(α*nΛ⊗v) )dΛ -
  #   ∫( meanᵧ(α,p*Id) ⊙ jump(α*nΛ⊗v) )dΛ +
  #   ∫( meanᵧ(α,σ(∇(v)) + q*Id) ⊙ (jump(α*nΛ)⊗meanᵧ(α,sᵤ(u))) -
  #      (γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(v))⋅meanᵧ(α,sᵤ(u))) -
  #      β₂*αₚ*( h*(jump(nΛ⋅∇(u))⋅jump(nΛ⋅∇(v)) ) +
  #             h3*((nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(u)))) ⋅ (nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(v)))) ) +
  #             h*(jump(p)⊙jump(q)) +
  #             h3*(jump(nΛ⋅∇(p))⊙jump(nΛ⋅∇(q))) ) -
  #   β₃*αₑ*( h*(jump(∇(u))⊙jump(∇(v)) ) +
  #           h3*(jump(∇∇(u))⊙jump(∇∇(v)) ) +
  #           h*(jump(p)⊙jump(q)) +
  #           h3*(jump(∇(p))⊙jump(∇(q))) ) )dΛ
  # l((v,q)) =
  #   ∫(α*(f⋅v))dΩ -
  #   ∫( meanᵧ(α,σ(∇(v)) + q*Id)⊙(jump(α*nΛ)⊗meanᵧ(α,u₀d)) -
  #     (γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(v))⋅meanᵧ(α,u₀d)) )dΛ
  a(::Val{1},(u,p),(v,q)) = ∫( α*( σ(ε(u))⊙ε(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ + ∫( α*( q*(∇⋅u) ) )dΩ -
    ∫( meanᵧ(α,σ(ε(u)) - p*Id)⊙jump(α*nΛ⊗v) )dΛ -
    ∫( (meanᵧ(α,σ(ε(v)) - q*Id)⋅jump(α*nΛ))⋅meanᵧ(α,sᵤ(u)) )dΛ +
    ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v))))⋅(meanᵧ(α,sᵤ(u))) )dΛ +
    ∫( (β₂*αₚ+β₃*αₑ)*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q)) ) )dΛ 
  # a(::Val{1},(u,p),(v,q)) = ∫( α*( σ(∇(u))⊙∇(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ - ∫( α*( ∇(q)⋅u) )dΩ -
  #   ∫( meanᵧ(α,σ(∇(u)) - p*Id)⊙jump(α*nΛ⊗v) )dΛ -
  #   ∫( (meanᵧ(α,σ(∇(v)))⋅jump(α*nΛ))⋅meanᵧ(α,sᵤ(u)) )dΛ +
  #   # ∫( (jump(α*q*nΛ))⋅meanᵧ(α,sᵤ(u)) )dΛ +
  #   # ∫( (jump(α*q*nΛ))⋅mean(u) )dΛ +
  #   ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v))))⋅(meanᵧ(α,sᵤ(u))) )dΛ +
  #   ∫(  β₂*αₚ*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q)) ) )dΛ +
  #   ∫(  β₃*αₑ*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q)) ) )dΛ
  a(::Val{2},(u,p),(v,q)) = ∫( α*( σ(ε(u))⊙∇(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ + ∫( α*( q*(∇⋅u) ) )dΩ -
    ∫( meanᵧ(α,σ(ε(u)) - p*Id)⊙jump(α*nΛ⊗v) )dΛ -
    ∫( (meanᵧ(α,σ(ε(v)) - q*Id)⋅jump(α*nΛ))⋅meanᵧ(α,sᵤ(u)) )dΛ +
    ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v))))⋅(meanᵧ(α,sᵤ(u))) )dΛ + 
    ∫( (β₂*αₚ+β₃*αₑ)*( h*(jump(nΛ⋅∇(u))⊙jump(nΛ⋅∇(v))) + h3*((nΛ.⁺⋅(jump(nΛ⋅∇∇(u))))⋅(nΛ.⁺⋅(jump(nΛ⋅∇∇(v))))) ) )dΛ + 
    ∫( 1.0e3*(β₂*αₚ+β₃*αₑ)*( h3*(jump(nΛ⋅∇(p))⊙jump(nΛ⋅∇(q))) ) )dΛ 
  l((v,q)) =
    ∫(α*(f⋅v))dΩ + ∫( v⋅g )dΓ -
    ∫( (meanᵧ(α,σ(ε(v)) - q*Id)⋅jump(α*nΛ))⋅u₀d )dΛ +
    ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v)⋅u₀d))) )dΛ
  # l((v,q)) =
  #   ∫(α*(f⋅v))dΩ -#+ ∫( g(nΓ)⋅v )dΓ -
  #   ∫( (meanᵧ(α,σ(∇(v)))⋅jump(α*nΛ))⋅u₀d )dΛ -
  #   ∫( (jump(α*q*nΛ))⋅u₀d )dΛ +
  #   ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v)⋅u₀d))) )dΛ
  op = AffineFEOperator(((u,p),(v,q))->a(Val(order),(u,p),(v,q)),l,X,Y)

  # solution
  # A = get_matrix(op)
  # b = get_vector(op)
  # add gmres here!
  # ls = IS_GMRESSolver(;maxiter=10000,reltol=1.e-12,verbose=false)
  ls = LUSolver()
  uₕ,pₕ = solve(ls,op)
  verbose && writevtk(Ω,output_folder*"/solution",cellfields=["weights"=>α,"uₕ"=>uₕ,"u₀"=>u₀,"pₕ"=>pₕ,"p₀"=>p₀,"dcf"=>dcf,"u₀d"=>u₀d,"ϕ"=>params.ϕ])

  # Postprocess
  vol = (∑(∫(α_ref.≈1.0)dΩ))
  pₕ₀ = ∑(∫(α_ref*pₕ)dΩ)/vol
  p₀₀ = ∑(∫(α_ref*p₀)dΩ)/vol
  # p₀₀ = ∑(∫(p₀)dΩ)
  # pₕ₀ = ∑(∫(pₕ)dΩ)
  # p₀_(x) = p₀(x) - p₀₀
  eᵤl₂ = uₕ - u₀
  # eₚl₂ = (pₕ-pₕ₀) - p₀_
  eₚl₂ = pₕ-p₀
  l2normᵤ = √(∑(∫((α_ref.≈1.0)*(eᵤl₂⋅eᵤl₂))dΩ))#/vol
  l2normₚ = √(∑(∫((α_ref.≈1.0)*(eₚl₂⋅eₚl₂))dΩ))#/vol
  println("L2 norm velocity: ",l2normᵤ)
  println("L2 norm pressure: ",l2normₚ)
  println("Mean pressure: ",pₕ₀)
  println("Vol",vol)

  return l2normᵤ,l2normₚ

end
