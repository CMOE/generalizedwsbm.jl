"""
NavierStokesParams

Parameters for the linear NavierStokes problem.
"""
@with_kw struct NavierStokesParams
  domain::Tuple{Float64,Float64,Float64,Float64} = (0.0,1.0,0.0,1.0)
  n_cells::Tuple{Int64,Int64} = (10,10)
  ϕ::Function = level_set(CircleParams())
  order::Int64 = 1 # FE order
  weight_quad_degree::Int64 = 20 # Quadrature degree for computing weights
  weight_min_value::Float64 = 0.0 # Minimum value for weights
  weight_approach::Symbol = :standard # Approach for computing weights (:standard or :binary (0.0 or 1.0))
  output_folder::String = datadir("sims","NavierStokes") # Output folder
  verbose::Bool = true # Verbosity
  f::Function = x -> VectorValue(0.0,0.0) # Source term
  u₀::Function = x -> VectorValue(0.0,0.0) # Exact solution velocity
  p₀::Function = x -> 0.0 # Exact solution pressure
  ν::Float64 = 1.0 # viscosity
  β₁::Float64 = 1.0 # Penalty parameter
  β₂::Float64 = 1.0 # Penalty parameter
  β₃::Float64 = 1.0 # Penalty parameter
  Cinv::Vector{Float64} = [36.0,36.0] # Inverse estimate constant per order
end

"""
    main_NavierStokes()

Main function for the NavierStokes problem. It computes the solution to the NavierStokes problem
on an arbitrary shaped domain with Dirichlet boundary conditions. The solution is computed using
the GeneralizedWSBM method, assuming the boundary is defined through a level set function.
"""
function main_navier_Stokes(params::NavierStokesParams)

  # General parameters
  @unpack output_folder, verbose = params

  # Discrete module
  @unpack domain, n_cells = params
  model = CartesianDiscreteModel(domain,n_cells)
  Ω = Interior(model)
  Λ = Skeleton(Ω)

  # Compute weights
  α = compute_weights(Ω,params)

  # Define FE spaces
  @unpack order = params
  @unpack f, u₀, p₀, ν = params
  D = num_dims(Ω)
  reffeᵤ = ReferenceFE(lagrangian,VectorValue{D,Float64},order)
  reffeₚ = ReferenceFE(lagrangian,Float64,order-1,space=:P)
  V = TestFESpace(Ω,reffeᵤ,conformity=:H1,dirichlet_tags="boundary")
  Q = TestFESpace(Ω,reffeₚ,conformity=:L2)#,constraint=:zeromean)
  U = TrialFESpace(V,u₀)
  P = TrialFESpace(Q)
  Y = MultiFieldFESpace([V,Q])
  X = MultiFieldFESpace([U,P])

  # Define measures
  dΩ = Measure(Ω,2*order)
  dΛ = Measure(Λ,2*order)
  nΛ = get_normal_vector(Λ)

  # Auxiliar variables
  Id = TensorValue(Matrix(1.0I,D,D))
  σ(∇u) = ν*(∇u)
  h = mean(CellField(lazy_map(vol->(vol)^(1/D),get_cell_measure(Ω)),Ω))
  h3 = mean(CellField(lazy_map(vol->((vol)^(1/D))^3,get_cell_measure(Ω)),Ω))
  αₚ = 1/ν * (mean(α) .< 1.0) * ( abs(jump(α)) .!= 2*mean(α) )
  αₑ = 1/ν * (mean(α).== jump(α))
  dcf = CellField(d(params),Ω)
  u₀d = CellField(x->u₀(x+d(params)(x)),Ω)
  sᵤ(u) = u+dcf⋅∇(u)+1/2*(dcf⋅(dcf⋅∇∇(u)))
  sᵤ¹(u) = u+dcf⋅∇(u)

  # Define variational problem
  @unpack β₁, β₂, β₃, Cinv = params
  η = 1/(8*√(Cinv[order]))*(-4+√(Cinv[order])+(65*Cinv[order]+56*√(Cinv[order])+16)^(1/2))
  γ = β₁*(order+1)^2*η*ν # rectangles
  # γ = β₁*(order+1)*(order+2)/2*η*ν #triangres
  # a((u,p),(v,q)) =
  #   ∫( α*( σ(∇(u))⊙∇(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ + ∫( α*( q*(∇⋅u) ) )dΩ -
  #   ∫( meanᵧ(α,σ(∇(u))) ⊙ jump(α*nΛ⊗v) )dΛ -
  #   ∫( meanᵧ(α,p*Id) ⊙ jump(α*nΛ⊗v) )dΛ +
  #   ∫( meanᵧ(α,σ(∇(v)) + q*Id) ⊙ (jump(α*nΛ)⊗meanᵧ(α,sᵤ(u))) -
  #      (γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(v))⋅meanᵧ(α,sᵤ(u))) -
  #      β₂*αₚ*( h*(jump(nΛ⋅∇(u))⋅jump(nΛ⋅∇(v)) ) +
  #             h3*((nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(u)))) ⋅ (nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(v)))) ) +
  #             h*(jump(p)⊙jump(q)) +
  #             h3*(jump(nΛ⋅∇(p))⊙jump(nΛ⋅∇(q))) ) -
  #   β₃*αₑ*( h*(jump(∇(u))⊙jump(∇(v)) ) +
  #           h3*(jump(∇∇(u))⊙jump(∇∇(v)) ) +
  #           h*(jump(p)⊙jump(q)) +
  #           h3*(jump(∇(p))⊙jump(∇(q))) ) )dΛ
  # l((v,q)) =
  #   ∫(α*(f⋅v))dΩ -
  #   ∫( meanᵧ(α,σ(∇(v)) + q*Id)⊙(jump(α*nΛ)⊗meanᵧ(α,u₀d)) -
  #     (γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(v))⋅meanᵧ(α,u₀d)) )dΛ
  c(a,u,v) = ∫( α*((a⋅∇(u))⋅v) )dΩ
  a(::Val{1},(u,p),(v,q)) = ∫( α*( σ(∇(u))⊙∇(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ + ∫( α*( q*(∇⋅u) ) )dΩ -
    ∫( meanᵧ(α,σ(∇(u)) - p*Id)⊙jump(α*nΛ⊗v) )dΛ -
    ∫( (meanᵧ(α,σ(∇(v)) - q*Id)⋅jump(α*nΛ))⋅meanᵧ(α,sᵤ(u)) )dΛ +
    ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v))))⋅(meanᵧ(α,sᵤ(u))) )dΛ +
    ∫(  β₂*αₚ*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q)) ) )dΛ +
    ∫(  β₃*αₑ*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q)) ) )dΛ
  a(::Val{2},(u,p),(v,q)) = ∫( α*( σ(∇(u))⊙∇(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ + ∫( α*( q*(∇⋅u) ) )dΩ -
    ∫( meanᵧ(α,σ(∇(u)) - p*Id)⊙jump(α*nΛ⊗v) )dΛ -
    ∫( (meanᵧ(α,σ(∇(v)) - q*Id)⋅jump(α*nΛ))⋅meanᵧ(α,sᵤ(u)) )dΛ +
    ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v))))⋅(meanᵧ(α,sᵤ(u))) )dΛ +
    ∫(  β₂*αₚ*( h*(jump(∇(u))⊙jump(∇(v))) + h3*(jump(∇∇(u))⊙jump(∇∇(v))) + h*(jump(p)⊙jump(q)) + h3*(jump(∇(p))⊙jump(∇(q))) ) )dΛ +
    ∫(  β₃*αₑ*( h*(jump(∇(u))⊙jump(∇(v))) + h3*(jump(∇∇(u))⊙jump(∇∇(v))) + h*(jump(p)⊙jump(q)) + h3*(jump(∇(p))⊙jump(∇(q))) ) )dΛ
  l((v,q)) =
    ∫(α*(f⋅v))dΩ -#+ ∫( g(nΓ)⋅v )dΓ -
    ∫( (meanᵧ(α,σ(∇(v)) - q*Id)⋅jump(α*nΛ))⋅u₀d )dΛ +
    ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v)⋅u₀d))) )dΛ
  res((u,p),(v,q)) = a(Val(order),(u,p),(v,q)) + c(a,u,v) - l((v,q))
  jac((u,p),(du,dp),(v,q)) = a(Val(order),(du,dp),(v,q)) + c(du,u,v) + c(u,du,v)
  op = FEOperator(res,jac,X,Y)

  # solution
  # A = get_matrix(op)
  # b = get_vector(op)
  # add gmres here!
  ls = IS_GMRESSolver(;maxiter=10000,reltol=1.e-12,verbose=false)
  # ls = LUSolver()
  uₕ,pₕ = solve(ls,op)
  verbose && writevtk(Ω,output_folder*"/solution",cellfields=["weights"=>α,"uₕ"=>uₕ,"u₀"=>u₀,"pₕ"=>pₕ,"p₀"=>p₀,"dcf"=>dcf,"u₀d"=>u₀d])

  # Postprocess
  pₕ₀ = ∑(∫(α*pₕ)dΩ)
  p₀₀ = ∑(∫(α*p₀)dΩ)
  p₀_(x) = p₀(x) - p₀₀
  eᵤl₂ = uₕ - u₀
  eₚl₂ = (pₕ-pₕ₀) - p₀_
  l2normᵤ = √(∑(∫((α.≈1.0)*(eᵤl₂⋅eᵤl₂))dΩ))
  l2normₚ = √(∑(∫((α.≈1.0)*(eₚl₂⋅eₚl₂))dΩ))

  return l2normᵤ,l2normₚ

end
