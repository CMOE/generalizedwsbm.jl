module TMP_Transient_navier_stokes
using DrWatson
@quickactivate "GeneralizedWSBM"
using GeneralizedWSBM
using Gridap
using TimerOutputs
using Plots
using Parameters
using LinearAlgebra
using GridapSolvers
using GridapSolvers.LinearSolvers
using LineSearches: BackTracking
using ProfileView

using Gridap.CellData: GenericField, Fill, DomainContribution, add_contribution!
using Gridap.Fields: IntegrationMap, BroadcastingFieldOpMap
using Gridap.Helpers: @notimplemented

include("src/HelperFunctions.jl")
include("src/weak_form.jl")

to = TimerOutput()

domain = (-0.75,2.5,-0.5,1.5)
# center(t) = [0.3*(t>0.3)*sin(2π/1.5*(t-0.3))+0.5,0.1*(t>0.4)*sin(2π/1.5*(t-0.4))+0.5]
# radius(t) = 0.3#+0.1*sin(2π/2*t)
center(t) = [0.2*sin(2π*t)+0.5,0.1*cos(2π*t)+0.5]
radius(t) = 0.3+0.1*sin(2π*t)
n_petals = 5
uin(x,t) = VectorValue(1.0,0.0)
uin(t::Real) = x -> uin(x,t)
# u(x) = VectorValue(0.0,0.0)
u(x,t) = VectorValue(0.5*π*cos(π*t),0.1*π*sin(π*t))
p(x) = 0.0
ν = 0.01
I = TensorValue(1.0,0.0,0.0,1.0)
f(x) = VectorValue(0.0,0.0)
c1(t) = [0.5+0.5*sin(π*t),0.4-0.1*cos(π*t)]
R1 = 0.4
R2 = 0.25
R3 = 0.6
R4 = 0.5
v1(t) = c1(t) + R1*[sin(0.0π*t-3π/4),cos(0.0π*t-3π/4)]
v2(t) = c1(t) + R2*[sin(0.0π*t+2π/3),cos(0.0π*t+2π/3)]
v3(t) = c1(t) + R3*[sin(0.0π*t+π/3),cos(0.0π*t+π/3)]
v4(t) = c1(t) + R4*[sin(0.0π*t-π/5),cos(0.0π*t-π/5)]

# Warm-up parameters
# ϕ(t) = level_set(FlowerParams(center=center(t),radius=radius(t),n=n_petals,in_out=1))
# ϕ(t) = level_set(CircleParams(center=center(t),radius=radius(t),in_out=1))
ϕ(t) = level_set(ParallelogramParams(v₁=v1(t),v₂=v2(t),v₃=v3(t),v₄=v4(t),in_out=-1))
n_cells = (50,25)
# n_cells = (40,20)
output_folder = "."
params(t) = NavierStokesParams(domain=domain,ϕ=ϕ(t),f=f,u₀=u,p₀=p,ν=ν,n_cells=n_cells,output_folder=output_folder,
                      order=1,weight_quad_degree=100,weight_approach=:standard,β₁=1)

function main_tmp(params,ft)
  # Discrete module
  @unpack domain, n_cells = params(0.0)
  model = CartesianDiscreteModel(domain,n_cells)
  Ω = Interior(model)
  Λ = Skeleton(Ω)

  # Compute weights
  α(t) = compute_weights(Ω,params(t))
  writevtk(Ω,"weights",cellfields=["α"=>α(0.0)])

  # Dirichlet boundaries
  labels = get_face_labeling(model)
  add_tag_from_tags!(labels,"Dirichlet",[1,2,3,4,5,6,7])
  add_tag_from_tags!(labels,"Neumann",[8])
  Γ = Boundary(Ω,tags="Neumann")

  # Define FE spaces
  @unpack order = params(0.0)
  @unpack f, u₀, p₀, ν = params(0.0)
  D = num_dims(Ω)
  reffeᵤ = ReferenceFE(lagrangian,VectorValue{D,Float64},order)
  reffeₚ = ReferenceFE(lagrangian,Float64,order-1)#,space=:P)
  V = TestFESpace(Ω,reffeᵤ,conformity=:H1,dirichlet_tags="Dirichlet")
  if order == 1
    Q = TestFESpace(Ω,reffeₚ,conformity=:L2)#,constraint=:zeromean)
  else
    Q = TestFESpace(Ω,reffeₚ,conformity=:C0)#,constraint=:zeromean)
  end
  U = TransientTrialFESpace(V,uin)
  P = TrialFESpace(Q)
  Y = MultiFieldFESpace([V,Q])
  X = TransientMultiFieldFESpace([U,P])

  # Define measures
  dΩ = Measure(Ω,2*order)
  dΛ = Measure(Λ,2*order)
  nΛ = get_normal_vector(Λ)

  buffer = Ref{Any}((α=nothing,t=nothing))
  function α_(t)
    if buffer[].t == t
      return buffer[].α
    else
      println("Computing α")
      buffer[] = (α=α(t),t=t)
      return buffer[].α
    end
  end

  buffer_d = Ref{Any}((d=nothing,t=nothing))
  function dcf(t)
    if buffer_d[].t == t
      return buffer_d[].d
    else
      println("Computing d")
      buffer_d[] = (d=CellField(d(params(t)),Ω),t=t)
      return buffer_d[].d
    end
  end

  # Auxiliar variables
  Id = TensorValue(1.0, 0.0, 0.0, 1.0)
  σ(∇u) = ν*(∇u)
  h = mean(CellField(lazy_map(vol->(vol)^(1/D),get_cell_measure(Ω)),Ω))
  h3 = mean(CellField(lazy_map(vol->((vol)^(1/D))^3,get_cell_measure(Ω)),Ω))
  αₚ(t) = (mean(α_(t)) .< 1.0) * ( abs(jump(α_(t))) .!= 2*mean(α_(t)) )
  αₑ(t) = (mean(α_(t)).== jump(α_(t)))
  # dcf(t) = CellField(d(params(t)),Ω)
  u₀d(t) = CellField(x->u₀(x+d(params(t))(x),t),Ω)
  sᵤ(u,t) = u+(dcf(t)⋅∇(u))#+1/2*(dcf(t)⋅(dcf(t)⋅∇∇(u)))
  sᵤ¹(u,t) = u+(dcf(t)⋅∇(u))
  sᵤ(u,∇u,d) = u+d⋅∇u
  # sᵤ¹(u) = u+dcf⋅∇u

  # Define variational problem
  @unpack β₁, β₂, β₃, Cinv = params(0.0)
  η = 1/(8*√(Cinv[order]))*(-4+√(Cinv[order])+(65*Cinv[order]+56*√(Cinv[order])+16)^(1/2))
  γ = β₁*(order+1)^2*η*ν # rectangles
  # γ = β₁*(order+1)*(order+2)/2*η*ν #triangres

  # Stabilization parameters
  c₁ = 12.0
  c₂ = 4.0
  cc = 4.0
  hτ = (domain[2]-domain[1])/n_cells[1]
  hτ2 = hτ^2
  abs_(u) = (u⋅u).^(1/2)+1.0e-14
  dabs_(u,du) = (u⋅du)/abs_(u)
  τₘ⁻¹(u) = (c₁*ν/hτ2 + c₂*(abs_(u))/hτ)
  τₘ(u) = 1/τₘ⁻¹(u)
  τc(u) = cc *(hτ2/(c₁*τₘ(u)))
  dτₘ(u,du) = -1.0/(τₘ⁻¹(u)*τₘ⁻¹(u)) * (c₂*(dabs_(u,du)))
  dτc(u,du) = -cc*hτ2/c₁ * (1/(τₘ(u)*τₘ(u))) * dτₘ(u,du)
  τₘα(α,u) = α*τₘ(u)
  vα(α,v) = α*v
  # s(t,a,u,v) = ∫( (((τ∘(a))*(α_(t)))*((a⋅∇(u))⊗a)⊙(∇(v))) )dΩ
  # ds(t,a,da,u,du,v) = ∫( (α_(t))*((dτ(a,da))*((a⋅∇(u))⋅(a⋅∇(v))) +
  #                       (τ(a))*((da⋅∇(u))⋅(a⋅∇(v)) + (a⋅∇(du))⋅(a⋅∇(v)) + (a⋅∇(u))⋅(da⋅∇(v)) )) )dΩ

  # TMP weak form
  # stab_expl_(u,p,v,q,t) = ∫( τₘ(u,h,h2) * ((∇(u)'⋅u + ∇(p) - η(uₙ(u,t),t))⋅(∇(v)'⋅u)+∇(q)) )dΩ_f
  𝒞ᵤ(a,∇u) = a⋅∇u
  ℒᵤ(a,∇u,∇p) = 𝒞ᵤ(a,∇u) + ∇p
  𝒫ᵤ(a,∇u,∇p,η) = ℒᵤ(a,∇u,∇p) - η
  m_α(α,uₜ,v) = α*(uₜ⋅v)
  conv(a,v,∇u) =  𝒞ᵤ(a,∇u)⋅v
  skew_conv(α,a,u,v,∇u,∇v) =  α*(0.5*(𝒞ᵤ(a,∇u)⋅v - 𝒞ᵤ(a,∇v)⋅u))
  sym_lapl(εu,εv) = 2ν*(εu⊙εv)
  div_term(divu,q) = divu*q
  skew_conv_Γ(α,a,u,v,n) = α*((u⋅v)*(0.5*(a⋅n)-neg(a⋅n)))
  σ_α(α,ε) = 2ν*(α*ε)
  # penalty(τ,u₀,u,v) = (τ⋅(u-u₀))⋅v
  # dpenalty(τ,du,v) = (τ⋅du)⋅v
  # complementary_uv(u₀,u,εv,n) = 2ν_f * ((n⋅εv)⋅(u-u₀))
  # complementary_uq(u₀,u,q,n) = 2ν_f * ((q*n)⋅(u-u₀))
  # dcomplementary_uv(du,εv,n) = 2ν_f * ((n⋅εv)⋅(du))
  # dcomplementary_uq(du,q,n) = 2ν_f * ((q*n)⋅(du))
  # u0cf(t) = CellField(u0(t),Γ_S)

  aΛ(t,u,p,v,q) = aΛ_(ν,γ,h,h3,β₂,β₃,α_(t),αₚ(t),αₑ(t),u,p,sᵤ(u,t),sᵤ¹(v,t),v,q,nΛ,dΛ)
  lΛ(t,v,q) = lΛ_(ν,γ,h,α_(t),u₀d(t),sᵤ¹(v,t),v,q,nΛ,dΛ)
  mass(t,(∂ₜu,),(v,)) = ∫( m_α(α_(t),∂ₜu,v) )dΩ
  res₀(t,(u,p),(v,q)) =
    ∫( (σ_α∘(α_(t),ε(u)))⊙ε(v) )dΩ +
    ∫( α_(t)*(div_term∘((∇⋅u),q)) )dΩ -
    ∫( α_(t)*(div_term∘((∇⋅v),p)) )dΩ +
    aΛ(t,u,p,v,q)
  res(t,(u,p),(v,q)) = res₀(t,(u,p),(v,q)) - lΛ(t,v,q) +
    ∫( α_(t)*(conv∘(u,v,∇(u))) )dΩ #+
    # ∫( α_(t)*((τₘ∘(u)) * ((ℒᵤ∘(u,∇(u),∇(p)))⋅(𝒞ᵤ∘(u,∇(v))))) )dΩ +
    # ∫( α_(t)*((τₘ∘(u)) * ((ℒᵤ∘(u,∇(u),∇(p)))⋅(∇(q)))) )dΩ +
    # ∫( α_(t)*((τc∘(u)) * ((∇⋅u)*(∇⋅v))) )dΩ
  jac(t,(u,p),(du,dp),(v,q)) =
    ∫( (σ_α∘(α_(t),ε(du)))⊙ε(v) )dΩ +
    ∫( α_(t)*((𝒞ᵤ∘(u,∇(du)))⋅v) )dΩ +
    ∫( α_(t)*((𝒞ᵤ∘(du,∇(u)))⋅v) )dΩ +
    ∫( α_(t)*(div_term∘((∇⋅du),q)) )dΩ -
    ∫( α_(t)*(div_term∘((∇⋅v),dp)) )dΩ +
    # ∫( α_(t)*((τₘ∘(u)) * (((ℒᵤ(du,∇(u),∇(dp)))⋅((𝒞ᵤ∘(u,∇(v)))+∇(q))))) )dΩ +
    # ∫( α_(t)*((τₘ∘(u)) * (((𝒞ᵤ∘(u,∇(du)))⋅((𝒞ᵤ∘(u,∇(v)))+∇(q))))) )dΩ +
    # ∫( α_(t)*((τₘ∘(u)) * (((ℒᵤ(du,∇(u),∇(dp))+(𝒞ᵤ∘(u,∇(du))))⋅((𝒞ᵤ∘(u,∇(v)))+∇(q))))))dΩ +
    # ∫( α_(t)*((τₘ∘(u)) * (((ℒᵤ(u,∇(u),∇(p)))⋅((𝒞ᵤ∘(du,∇(v))))))) )dΩ +
    # ∫( α_(t)*(τₘ∘(u)) *( ( (((ℒᵤ(du,∇(u),∇(dp))+(𝒞ᵤ∘(u,∇(du))))⋅((𝒞ᵤ∘(u,∇(v)))+∇(q)))) +
    #                        (((ℒᵤ(u,∇(u),∇(p)))⋅((𝒞ᵤ∘(du,∇(v)))))))) )dΩ +
    # ∫( α_(t)*((dτₘ∘(u,du)) * ((ℒᵤ(u,∇(u),∇(p)))⋅((𝒞ᵤ∘(u,∇(v)))+∇(q)))) )dΩ +
    # ∫( α_(t)*((τc∘(u)) * ((∇⋅du)*(∇⋅v))) )dΩ +
    # ∫( α_(t)*(dτc∘(u,du)) * ((∇⋅u)*(∇⋅v)) )dΩ +
    aΛ(t,du,dp,v,q)
  jac_t(t,(u,),(dut,),(v,)) = mass(t,(dut,),(v,))
  a₀((u,p),(v,q)) = res₀(0, (u,p),(v,q))
  l((v,q)) = lΛ(0,v,q)

  op₀ = AffineFEOperator(a₀,l,X(0.0),Y)
  op = TransientSemilinearFEOperator(mass,res,(jac,jac_t),X,Y)

  # solution
  # ls = IS_GMRESSolver(;maxiter=10000,reltol=1.e-12,verbose=false)
  ls = LUSolver()
  nls = NLSolver(ls,show_trace=true,iterations=15,ftol=1.0e-5)
  xₕ₀ = solve(op₀)
  ode_solver = ThetaMethod(nls,0.02,1.0)
  xₕₜ = solve(ode_solver,op,0.0,ft,xₕ₀)
  # writevtk(Ω,output_folder*"/solution",cellfields=["weights"=>α,"uₕ"=>uₕ,"u₀"=>u₀,"pₕ"=>pₕ,"p₀"=>p₀,"dcf"=>dcf,"u₀d"=>u₀d])

  # Postprocess
  createpvd("tmp_transient_navier_stokes") do pvd
    for (t,(uh,ph)) in xₕₜ
      println("time: ",t)
      pvd[t] = createvtk(Ω,"tmp_transient_navier_stokes_$t",cellfields=["u"=>uh,"p"=>ph,"α"=>α_(t),"ϕ"=>ϕ(t)],order=order)
    end
  end

  return nothing

end

@timeit to "main1" main_tmp(params,0.02)
# params(t) = NavierStokesParams(domain=domain,ϕ=ϕ(t),f=f,u₀=u,p₀=p,ν=ν,n_cells=n_cells,output_folder=output_folder,
#                       order=2,weight_quad_degree=100,weight_approach=:standard,β₁=1)

# @timeit to "main2" main_tmp(params,0.02)
# # @profview main_tmp(params,0.05)
n_cells = (100,50)
params(t) = NavierStokesParams(domain=domain,ϕ=ϕ(t),f=f,u₀=u,p₀=p,ν=ν,n_cells=n_cells,output_folder=output_folder,
                      order=1,weight_quad_degree=100,weight_approach=:standard,β₁=1)
@timeit to "main2" main_tmp(params,10.0)

show(to)

end # module
