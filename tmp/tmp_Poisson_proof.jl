a(u,v) =
∫(α*(∇(u)⋅∇(v)))dΩ -
∫( meanᵧ(α,∇(u))⋅jump(α*v*nΛ) +
   meanᵧ(α,∇(v))⋅jump(α*nΛ)*meanᵧ(α,sᵤ(u)) -
  (γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(v))*meanᵧ(α,sᵤ(u))) -
  β₂*αₚ*h*(jump(nΛ⋅∇(u))⋅jump(nΛ⋅∇(v)) ) -
  β₂*αₚ*h3*( (nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(u)))) * (nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(v)))) ) -
  β₃*αₑ*h*(jump(∇(u))⋅jump(∇(v)) ) -
  β₃*αₑ*h3*( jump(∇∇(u)) ⊙ jump(∇∇(v)) ))dΛ

  # u->V
  a(u,u) =
  ∫(α*(∇(u)⋅∇(u)))dΩ -
  ∫( meanᵧ(α,∇(u))⋅jump(α*u*nΛ) +
     meanᵧ(α,∇(u))⋅jump(α*nΛ)*meanᵧ(α,sᵤ(u)) -
    (γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(u))*meanᵧ(α,sᵤ(u))) -
    β₂*αₚ*h*(jump(nΛ⋅∇(u))⋅jump(nΛ⋅∇(u)) ) -
    β₂*αₚ*h3*( (nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(u)))) * (nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(u)))) ) -
    β₃*αₑ*h*(jump(∇(u))⋅jump(∇(u)) ) -
    β₃*αₑ*h3*( jump(∇∇(u)) ⊙ jump(∇∇(u)) ))dΛ

# Norms
  a(u,u) = \|√α ∇(u)\|²_Ω -
  ∫( meanᵧ(α,∇(u))⋅jump(α*nΛ)*meanᵧ(α,dcf⋅∇(u)) dΩ
  + \|√(γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(u))\| -
    (γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(u))*meanᵧ(α,sᵤ(u))) -
    β₂*αₚ*h*(jump(nΛ⋅∇(u))⋅jump(nΛ⋅∇(u)) ) -
    β₂*αₚ*h3*( (nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(u)))) * (nΛ.⁺⋅(nΛ.⁺⋅jump(∇∇(u)))) ) -
    β₃*αₑ*h*(jump(∇(u))⋅jump(∇(u)) ) -
    β₃*αₑ*h3*( jump(∇∇(u)) ⊙ jump(∇∇(u)) ))dΛ
