module TMP_Transient_Stokes_divu
using DrWatson
@quickactivate "GeneralizedWSBM"
using GeneralizedWSBM
using Gridap
using TimerOutputs
using Plots
using Parameters
using LinearAlgebra
using GridapSolvers
using GridapSolvers.LinearSolvers

include("src/HelperFunctions.jl")

to = TimerOutput()

domain = (-0.5,2.5,-0.5,1.5)
center(t) = [0.2*sin(2π*t)+0.5,0.1*cos(2π*t)+0.5]
radius(t) = 0.3+0.1*sin(2π*t)
n_petals = 5
uin(x,t) = VectorValue(1.0,0.0)
uin(t::Real) = x -> uin(x,t)
u(x) = VectorValue(0.0,0.0)
p(x) = 0.0
ν = 1.0
I = TensorValue(1.0,0.0,0.0,1.0)
f(x) = VectorValue(0.0,0.0)
c1(t) = [0.5+0.5*sin(2π*t),0.5]
R1 = 0.4
R2 = 0.25
R3 = 0.6
R4 = 0.5
v1(t) = c1(t) + R1*[sin(2π*t-3π/4),cos(2π*t-3π/4)]
v2(t) = c1(t) + R2*[sin(2π*t+2π/3),cos(2π*t+2π/3)]
v3(t) = c1(t) + R3*[sin(2π*t+π/3),cos(2π*t+π/3)]
v4(t) = c1(t) + R4*[sin(2π*t-π/5),cos(2π*t-π/5)]

# Warm-up parameters
# ϕ(t) = level_set(FlowerParams(center=center(t),radius=radius(t),n=n_petals,in_out=1))
# ϕ(t) = level_set(CircleParams(center=center(t),radius=radius(t),in_out=1))
# ϕ(t) = level_set(ParallelogramParams(v₁=[.1,0.1],v₂=[1,0.5],v₃=[1,1],v₄=[0.5,1],in_out=-1))
ϕ(t) = level_set(ParallelogramParams(v₁=v1(t),v₂=v2(t),v₃=v3(t),v₄=v4(t),in_out=-1))
n_cells = (60,20)
output_folder = "."
params(t) = StokesParams(domain=domain,ϕ=ϕ(t),f=f,u₀=u,p₀=p,ν=ν,n_cells=n_cells,output_folder=output_folder,
                      order=1,weight_quad_degree=10,weight_approach=:standard,β₁=1)

function main_tmp(params,ft)
  # Discrete module
  @unpack domain, n_cells = params(0.0)
  model = CartesianDiscreteModel(domain,n_cells)
  Ω = Interior(model)
  Λ = Skeleton(Ω)

  # Compute weights
  α(t) = compute_weights(Ω,params(t))
  writevtk(Ω,"weights",cellfields=["α"=>α(0.0)])

  # Dirichlet boundaries
  labels = get_face_labeling(model)
  add_tag_from_tags!(labels,"Dirichlet",[1,2,3,4,5,6,7])
  add_tag_from_tags!(labels,"Neumann",[8])
  Γ = Boundary(Ω,tags="Neumann")

  # Define FE spaces
  @unpack order = params(0.0)
  @unpack f, u₀, p₀, ν = params(0.0)
  D = num_dims(Ω)
  reffeᵤ = ReferenceFE(lagrangian,VectorValue{D,Float64},order)
  reffeₚ = ReferenceFE(lagrangian,Float64,order-1)#,space=:P)
  V = TestFESpace(Ω,reffeᵤ,conformity=:H1,dirichlet_tags="Dirichlet")
  Q = TestFESpace(Ω,reffeₚ,conformity=:L2)#,constraint=:zeromean)
  U = TransientTrialFESpace(V,uin)
  P = TrialFESpace(Q)
  Y = MultiFieldFESpace([V,Q])
  X = TransientMultiFieldFESpace([U,P])

  # Define measures
  dΩ = Measure(Ω,2*order)
  dΛ = Measure(Λ,2*order)
  nΛ = get_normal_vector(Λ)

  # Auxiliar variables
  Id = TensorValue(1.0, 0.0, 0.0, 1.0)
  σ(∇u) = ν*(∇u)
  h = mean(CellField(lazy_map(vol->(vol)^(1/D),get_cell_measure(Ω)),Ω))
  h3 = mean(CellField(lazy_map(vol->((vol)^(1/D))^3,get_cell_measure(Ω)),Ω))
  αₚ(t) = 1/ν * (mean(α(t)) .< 1.0) * ( abs(jump(α(t))) .!= 2*mean(α(t)) )
  αₑ(t) = 1/ν * (mean(α(t)).== jump(α(t)))
  dcf(t) = CellField(d(params(t)),Ω)
  u₀d(t) = CellField(x->u₀(x+d(params(t))(x)),Ω)
  sᵤ(u,t) = u+dcf(t)⋅∇(u)#+1/2*(dcf⋅(dcf⋅∇∇(u)))
  sᵤ¹(u,t) = u+dcf(t)⋅∇(u)
  sᵤ(u,∇u,d) = u+d⋅∇u
  # sᵤ¹(u) = u+dcf⋅∇u

  # Define variational problem
  @unpack β₁, β₂, β₃, Cinv = params(0.0)
  η = 1/(8*√(Cinv[order]))*(-4+√(Cinv[order])+(65*Cinv[order]+56*√(Cinv[order])+16)^(1/2))
  γ = β₁*(order+1)^2*η*ν # rectangles
  # γ = β₁*(order+1)*(order+2)/2*η*ν #triangres

  # GWSBM
  # ----------------------------------
  aΩ(α) = α#*( ν*(∇u))⊙∇v#- (p*tr(∇v)) + (q*tr(∇u)) )
  m(t,(uₜ,),(v,)) = ∫( α(t)*(uₜ⋅v) )dΩ
  a(t,(u,p),(v,q)) =
    ∫( (aΩ∘(α(t)))*( σ∘(∇(u))⊙∇(v) ))dΩ - ∫( (aΩ∘(α(t)))*(( p*(∇⋅v) )) )dΩ + ∫( (aΩ∘(α(t)))*( q*(∇⋅u) ) )dΩ  + ∫( (1-α(t))*( (∇⋅v)*(∇⋅u) ) )dΩ -
    ∫( meanᵧ(α(t),σ(∇(u)) - p*Id)⊙jump(α(t)*nΛ⊗v) )dΛ -
    ∫( (meanᵧ(α(t),σ(∇(v)) - q*Id)⋅jump(α(t)*nΛ))⋅meanᵧ(α(t),sᵤ(u,t)) )dΛ +
    ∫( ((γ/h*abs(jump(α(t))))*(meanᵧ(α(t),sᵤ(v,t))))⋅(meanᵧ(α(t),sᵤ(u,t))) )dΛ +
    ∫(  β₂*αₚ(t)*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q)) ) )dΛ +
    ∫(  β₃*αₑ(t)*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q)) ) )dΛ
  l(t,(v,q)) =
    ∫(α(t)*(f⋅v))dΩ -#+ ∫( g(nΓ)⋅v )dΓ -
    ∫( (meanᵧ(α(t),σ(∇(v)) - q*Id)⋅jump(α(t)*nΛ))⋅u₀d(t) )dΛ +
    ∫( ((γ/h*abs(jump(α(t))))*(meanᵧ(α(t),sᵤ(v,t)⋅u₀d(t)))) )dΛ
  l((v,q)) =
    ∫(α(0.0)*(f⋅v))dΩ -#+ ∫( g(nΓ)⋅v )dΓ -
    ∫( (meanᵧ(α(0.0),σ(∇(v)) - q*Id)⋅jump(α(0.0)*nΛ))⋅u₀d(0.0) )dΛ +
    ∫( ((γ/h*abs(jump(α(0.0))))*(meanᵧ(α(0.0),sᵤ(v,0.0)⋅u₀d(0.0) ))) )dΛ

  res(t,(u,p),(v,q)) = a(t,(u,p),(v,q)) - l(t,(v,q))
  jac(t,(u,p),(du,dp),(v,q)) = a(t,(du,dp),(v,q))
  jac_t(t,(u,),(duₜ,),(v,)) = m(t,(duₜ,),(v,))

  op₀ = AffineFEOperator(((u,p),(v,q))->a(0.0,(u,p),(v,q)),l,X(0.0),Y)
  op = TransientLinearFEOperator((a,m),l,X,Y)

  # solution
  # ls = IS_GMRESSolver(;maxiter=10000,reltol=1.e-12,verbose=false)
  ls = LUSolver()
  xₕ₀ = solve(op₀)
  ode_solver = ThetaMethod(ls,0.001,1.0)
  xₕₜ = solve(ode_solver,op,0.0,ft,xₕ₀)
  # writevtk(Ω,output_folder*"/solution",cellfields=["weights"=>α,"uₕ"=>uₕ,"u₀"=>u₀,"pₕ"=>pₕ,"p₀"=>p₀,"dcf"=>dcf,"u₀d"=>u₀d])

  # Postprocess
  createpvd("tmp_transient_stokes_divu") do pvd
    for (t,(uh,ph)) in xₕₜ
      println("time: ",t)
      println("p-norm: ",∑(∫(α(t)*(ph*ph))dΩ))
      pvd[t] = createvtk(Ω,"tmp_transient_stokes_divu_$t",cellfields=["u"=>uh,"p"=>ph,"α"=>α(t),"ϕ"=>ϕ(t)])
    end
  end

  return nothing

end

@timeit to "main1" main_tmp(params,0.001)
@timeit to "main2" main_tmp(params,0.01)
# n_cells = (220,110)
# params(t) = StokesParams(domain=domain,ϕ=ϕ(t),f=f,u₀=u,p₀=p,ν=ν,n_cells=n_cells,output_folder=output_folder,
#                       order=1,weight_quad_degree=10,weight_approach=:standard,β₁=1)
# @timeit to "main3" main_tmp(params,3.0)

show(to)

end # module
