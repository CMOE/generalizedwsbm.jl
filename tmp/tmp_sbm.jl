module TMP_SBM
using DrWatson
@quickactivate "GeneralizedWSBM"
using GeneralizedWSBM
using Gridap
using TimerOutputs
using Plots
using Parameters
using LinearAlgebra
using GridapSolvers
using GridapSolvers.LinearSolvers

include("src/HelperFunctions.jl")

center = [0.5,0.5]
radius = 0.3
n_petals = 5
a₀ = 1/10
u₁(x) = 20*x[1]*x[2]^3
u₂(x) = 5*x[1]^4-5*x[2]^4
u(x) = VectorValue(u₁(x),u₂(x))
p(x) =  0.0#60*x[1]^2*x[2] - 20*x[2]^3
ν = 1.0
I = TensorValue(1.0,0.0,0.0,1.0)
f(x) = -ν*Δ(u)(x) + ∇(p)(x)#VectorValue(f₁(x),f₂(x)) + ∇(p)(x)
g(n) = x -> (ν*(∇(u)⋅n)(x)-(p*n)(x))

# Warm-up parameters
ϕ = level_set(CircleParams(center=center,radius=radius,in_out=1))
# ϕ = level_set(FlowerParams(center=center,radius=radius,n=n_petals))
n_cells = (20,20)
output_folder = "."
params = StokesParams(ϕ=ϕ,f=f,u₀=u,p₀=p,ν=ν,n_cells=n_cells,output_folder=output_folder,
                      order=1,weight_quad_degree=100,weight_approach=:standard,β₁=1)

function main_tmp(params)
  # Discrete module
  @unpack domain, n_cells = params
  model = CartesianDiscreteModel(domain,n_cells)
  Ω = Interior(model)
  Λ = Skeleton(Ω)

  # Compute weights
  α = compute_weights(Ω,params)
  writevtk(Ω,"weights",cellfields=["α"=>α])

  mask = [ αi.value == 1 for αi in Gridap.CellData.get_data(α)]
  mask_wsbm = [ 0.0 < αi.value < 1.0 for αi in Gridap.CellData.get_data(α)]
  not_mask = [ αi.value == 0 for αi in Gridap.CellData.get_data(α)]
  # println(mask_wsbm)
  Ωsbm = Interior(Ω,mask)
  Ωwsbm = Interior(Ω,mask_wsbm)
  Ωext = Interior(Ω,not_mask)
  Γsbm = Interface(Ωsbm,Ωext).⁺
  Γwsbm = Interface(Ωwsbm,Ωext).⁺
  Γinner = Interface(Ωsbm,Ωwsbm)
  Λwsbm = Skeleton(Ωwsbm)
  writevtk(Ωsbm,"sbm")
  writevtk(Ωwsbm,"wsbm")
  writevtk(Γsbm,"Gsbm")
  writevtk(Γwsbm,"Gwsbm")
  writevtk(Γinner,"Ginner")
  writevtk(Λwsbm,"Lwsbm")

  # Dirichlet boundaries
  labels = get_face_labeling(model)
  add_tag_from_tags!(labels,"Dirichlet",[1,2,3,4,5,6,7,8])
  add_tag_from_tags!(labels,"Neumann",[8])
  Γ = Boundary(Ω,tags="Neumann")

  # Define FE spaces
  @unpack order = params
  @unpack f, u₀, p₀, ν = params
  D = num_dims(Ω)
  reffeᵤ = ReferenceFE(lagrangian,VectorValue{D,Float64},order)
  reffeₚ = ReferenceFE(lagrangian,Float64,order-1)#,space=:P)
  V = TestFESpace(Ω,reffeᵤ,conformity=:H1,dirichlet_tags="Dirichlet")
  Q = TestFESpace(Ω,reffeₚ,conformity=:L2)#,constraint=:zeromean)
  # V = TestFESpace(Ωsbm,reffeᵤ,conformity=:H1,dirichlet_tags="Dirichlet")
  # Q = TestFESpace(Ωsbm,reffeₚ,conformity=:L2)#,constraint=:zeromean)
  U = TrialFESpace(V,u₀)
  P = TrialFESpace(Q)
  Y = MultiFieldFESpace([V,Q])
  X = MultiFieldFESpace([U,P])

  # Define measures
  dΩ = Measure(Ω,2*order)
  dΛ = Measure(Λ,2*order)
  nΛ = get_normal_vector(Λ)
  dΓ = Measure(Γ,2*order)
  nΓ = get_normal_vector(Γ)
  dΩsbm = Measure(Ωsbm,2*order)
  dΓsbm = Measure(Γsbm,2*order)
  nΓsbm = get_normal_vector(Γsbm)
  dΓwsbm = Measure(Γwsbm,2*order)
  nΓwsbm = get_normal_vector(Γwsbm)
  dΓinner = Measure(Γinner,2*order)
  nΓinner = get_normal_vector(Γinner)
  dΛwsbm = Measure(Λwsbm,2*order)
  nΛwsbm = get_normal_vector(Λwsbm)

  # Auxiliar variables
  Id = TensorValue(1.0, 0.0, 0.0, 1.0)
  σ(∇u) = ν*(∇u)
  hsbm = CellField(lazy_map(vol->(vol)^(1/D),get_cell_measure(Ω)),Ω)
  h = mean(CellField(lazy_map(vol->(vol)^(1/D),get_cell_measure(Ω)),Ω))
  h3 = mean(CellField(lazy_map(vol->((vol)^(1/D))^3,get_cell_measure(Ω)),Ω))
  αₚ = 1/ν * (mean(α) .< 1.0) * ( abs(jump(α)) .!= 2*mean(α) )
  αₑ = 1/ν * (mean(α).== jump(α))
  dcf = CellField(d(params),Ω)
  u₀d = CellField(x->u₀(x+d(params)(x)),Ω)
  sᵤ(u) = u+dcf⋅∇(u)#+1/2*(dcf⋅(dcf⋅∇∇(u)))
  sᵤ¹(u) = u+dcf⋅∇(u)
  sᵤ(u,∇u,d) = u+d⋅∇u
  # sᵤ¹(u) = u+dcf⋅∇u
  writevtk(Λ,"Lweights",cellfields=["α"=>meanᵧ(α,α),"ap"=>α.⁺,"am"=>α.⁻,"jump"=>jump(α),"jnΛ"=>jump(α*nΛ), "αₚ"=>αₚ,"αₑ"=>αₑ])


  # Define variational problem
  @unpack β₁, β₂, β₃, Cinv = params
  η = 1/(8*√(Cinv[order]))*(-4+√(Cinv[order])+(65*Cinv[order]+56*√(Cinv[order])+16)^(1/2))
  γ = β₁*(order+1)^2*η*ν # rectangles
  # γ = β₁*(order+1)*(order+2)/2*η*ν #triangres

  # GSBM
  # ----------------------------------
  # a((u,p),(v,q)) =
  #   ∫( α*( σ(∇(u))⊙∇(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ + ∫( α*( q*(∇⋅u) ) )dΩ -
  #   ∫( meanᵧ(α,σ(∇(u))) ⊙ jump(α*nΛ⊗v) )dΛ +
  #   ∫( meanᵧ(α,p*Id) ⊙ jump(α*nΛ⊗v) )dΛ -
  #   ∫( (meanᵧ(α,σ(∇(v)))⋅jump(α*nΛ)) ⋅ meanᵧ(α,sᵤ(u)) )dΛ +
  #   ∫( (meanᵧ(α,q)*jump(α*nΛ)) ⋅ meanᵧ(α,sᵤ(u)) )dΛ +
  #   ∫( (γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(v))⋅meanᵧ(α,sᵤ(u))) )dΛ +
  #   ∫(  β₃*αₑ*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q))) )dΛ
  # l((v,q)) =
  #   ∫(α*(f⋅v))dΩ -#+ ∫( g(nΓ)⋅v )dΓ -
  #   ∫( (meanᵧ(α,σ(∇(v)))⋅jump(α*nΛ)) ⋅ meanᵧ(α,u₀d) )dΛ +
  #   ∫( (meanᵧ(α,q)*jump(α*nΛ)) ⋅ meanᵧ(α,u₀d) )dΛ +
  #   ∫( (γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ¹(v))⋅meanᵧ(α,u₀d)) )dΛ

  # SBM
  # ----------------------------------
  # a((u,p),(v,q)) =
  #   ∫( σ(∇(u))⊙∇(v) )dΩsbm - ∫( p*(∇⋅v) )dΩsbm + ∫( q*(∇⋅u) )dΩsbm -
  #   ∫( (σ(∇(u))⋅nΓsbm - p*nΓsbm)⋅v )dΓsbm -
  #   ∫( (σ(∇(v))⋅nΓsbm - q*nΓsbm)⋅(sᵤ∘(u,∇(u),dcf)) )dΓsbm +
  #   ∫( (γ/hsbm*(sᵤ∘(v,∇(v),dcf)))⋅(sᵤ∘(u,∇(u),dcf)) )dΓsbm
  # l((v,q)) =
  #   ∫(f⋅v)dΩsbm -#+ ∫( g(nΓ)⋅v )dΓ -
  #   ∫( (σ(∇(v))⋅nΓsbm - q*nΓsbm)⋅u₀d )dΓsbm +
  #   ∫( (γ/hsbm*(sᵤ∘(v,∇(v),dcf)))⋅u₀d )dΓsbm

  # WSBM
  # ----------------------------------
  # a((u,p),(v,q)) = ∫( α*( σ(∇(u))⊙∇(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ + ∫( α*( q*(∇⋅u) ) )dΩ -
  #   ∫( α*(σ(∇(u))⋅nΓwsbm - p*nΓwsbm)⋅v )dΓwsbm -
  #   ∫( meanᵧ(α,σ(∇(u)) - p*Id)⊙jump(α*nΓinner⊗v) )dΓinner -
  #   ∫( meanᵧ(α,σ(∇(u)) - p*Id)⊙jump(α*nΛwsbm⊗v) )dΛwsbm -
  #   ∫( α*(σ(∇(v))⋅nΓwsbm - q*nΓwsbm)⋅(sᵤ∘(u,∇(u),dcf)) )dΓwsbm -
  #   ∫( (meanᵧ(α,σ(∇(v)) - q*Id)⋅jump(α*nΓinner))⋅meanᵧ(α,sᵤ(u)) )dΓinner -
  #   ∫( (meanᵧ(α,σ(∇(v)) - q*Id)⋅jump(α*nΛwsbm))⋅meanᵧ(α,sᵤ(u)) )dΛwsbm +
  #   ∫( ((γ/hsbm*abs(jump(α)))*(sᵤ∘(v,∇(v),dcf)))⋅(sᵤ∘(u,∇(u),dcf)) )dΓwsbm +
  #   ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v))))⋅(meanᵧ(α,sᵤ(u))) )dΓinner +
  #   ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v))))⋅(meanᵧ(α,sᵤ(u))) )dΛwsbm +
  #   ∫(  β₂*αₚ*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q))) )dΓinner +
  #   ∫(  β₂*αₚ*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q))) )dΛwsbm +
  #   ∫(  β₃*αₑ*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q))) )dΓinner +
  #   ∫(  β₃*αₑ*( h*(jump(∇(u))⊙jump(∇(v))) + h*(jump(p)⊙jump(q))) )dΛwsbm
  # l((v,q)) =
  #   ∫(α*(f⋅v))dΩ -#+ ∫( g(nΓ)⋅v )dΓ -
  #   ∫( α*(σ(∇(v))⋅nΓwsbm - q*nΓwsbm)⋅u₀d )dΓwsbm -
  #   ∫( (meanᵧ(α,σ(∇(v)) - q*Id)⋅jump(α*nΓinner))⋅u₀d )dΓinner -
  #   ∫( (meanᵧ(α,σ(∇(v)) - q*Id)⋅jump(α*nΛwsbm))⋅u₀d )dΛwsbm +
  #   ∫( ((γ/hsbm*abs(jump(α)))*(sᵤ∘(v,∇(v),dcf)))⋅u₀d )dΓwsbm +
  #   ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v)⋅u₀d))) )dΓinner +
  #   ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v)⋅u₀d))) )dΛwsbm

  # GWSBM
  # ----------------------------------
  a((u,p),(v,q)) = ∫( α*( σ(∇(u))⊙∇(v) ) )dΩ - ∫( α*( p*(∇⋅v) ) )dΩ + ∫( α*( q*(∇⋅u) ) )dΩ -
    ∫( meanᵧ(α,σ(∇(u)) - p*Id)⊙jump(α*nΛ⊗v) )dΛ -
    ∫( (meanᵧ(α,σ(∇(v)) - q*Id)⋅jump(α*nΛ))⋅meanᵧ(α,sᵤ(u)) )dΛ +
    ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v))))⋅(meanᵧ(α,sᵤ(u))) )dΛ +
    ∫(  β₂*αₚ*( h*(jump(∇(u))⊙jump(∇(v))) + 0.0*h3*(jump(∇∇(u))⊙jump(∇∇(v))) + h*(jump(p)⊙jump(q)) + 0.0*h3*(jump(∇(p))⊙jump(∇(q))) ) )dΛ +
    ∫(  β₃*αₑ*( h*(jump(∇(u))⊙jump(∇(v))) + 0.0*h3*(jump(∇∇(u))⊙jump(∇∇(v))) + h*(jump(p)⊙jump(q)) + 0.0*h3*(jump(∇(p))⊙jump(∇(q))) ) )dΛ
  l((v,q)) =
    ∫(α*(f⋅v))dΩ -#+ ∫( g(nΓ)⋅v )dΓ -
    ∫( (meanᵧ(α,σ(∇(v)) - q*Id)⋅jump(α*nΛ))⋅u₀d )dΛ +
    ∫( ((γ/h*abs(jump(α)))*(meanᵧ(α,sᵤ(v)⋅u₀d))) )dΛ


  op = AffineFEOperator(a,l,X,Y)

  # solution
  # A = get_matrix(op)
  # b = get_vector(op)
  # add gmres here!
  ls = IS_GMRESSolver(;maxiter=10000,reltol=1.e-12,verbose=false)
  # ls = LUSolver()
  uₕ,pₕ = solve(ls,op)
  writevtk(Ω,output_folder*"/solution",cellfields=["weights"=>α,"uₕ"=>uₕ,"u₀"=>u₀,"pₕ"=>pₕ,"p₀"=>p₀,"dcf"=>dcf,"u₀d"=>u₀d])

  # Postprocess
  pₕ₀ = ∑(∫(α*pₕ)dΩ)
  p₀₀ = ∑(∫(α*p₀)dΩ)
  p₀_(x) = p₀(x) - p₀₀
  eᵤl₂ = uₕ - u₀
  eₚl₂ = (pₕ-pₕ₀) - p₀_
  l2normᵤ = √(∑(∫((α.≈1.0)*(eᵤl₂⋅eᵤl₂))dΩ))
  l2normₚ = √(∑(∫((α.≈1.0)*(eₚl₂⋅eₚl₂))dΩ))

  return l2normᵤ,l2normₚ

end

main_tmp(params)

l2error = []

for n in [20,40,80]
    params = StokesParams(ϕ=ϕ,f=f,u₀=u,p₀=p,ν=ν,n_cells=(n,n),output_folder=output_folder,
    order=1,weight_quad_degree=100,weight_approach=:binary)
    push!(l2error,main_tmp(params)[1])
end

end # module
